#!/bin/sh
sed -i \
         -e 's/rgb(0%,0%,0%)/#1b293b/g' \
         -e 's/rgb(100%,100%,100%)/#d1c9c5/g' \
    -e 's/rgb(50%,0%,0%)/#1b293b/g' \
     -e 's/rgb(0%,50%,0%)/#986c80/g' \
 -e 's/rgb(0%,50.196078%,0%)/#986c80/g' \
     -e 's/rgb(50%,0%,50%)/#1b293b/g' \
 -e 's/rgb(50.196078%,0%,50.196078%)/#1b293b/g' \
     -e 's/rgb(0%,0%,50%)/#d1c9c5/g' \
	"$@"
