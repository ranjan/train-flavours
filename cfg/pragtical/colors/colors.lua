local style = require "core.style"
local common = require "core.common"

local bg       = { common.color "#1b293b" }
local bg2      = { common.color "#3f4957" }
local fg       = { common.color "#d1c9c5" }
local fgdim    = { common.color "#d8d1ce" }
local red      = { common.color "#8b6f8b" }
local green    = { common.color "#627c8c" }
local yellow   = { common.color "#6b7a85" }
local blue     = { common.color "#986c80" }
local magenta  = { common.color "#996d72" }
local cyan     = { common.color "#aa665e" }
local orange   = { common.color "#a47056" }
local gray     = { common.color "#646973" }
local graylight= { common.color "#ada9aa" }

style.background = bg
style.background2 = bg
style.background3 = bg2
style.text = fg
style.caret = red
style.accent = blue
style.dim = fgdim
style.divider = gray
style.selection = graylight
style.line_number = fgdim
style.line_number2 = fg
style.line_highlight = gray
style.scrollbar = fgdim
style.scrollbar2 = fg

style.syntax["normal"] = fg
style.syntax["symbol"] = fg
style.syntax["comment"] = fgdim
style.syntax["keyword"] = magenta
style.syntax["keyword2"] = red
style.syntax["number"] = blue
style.syntax["literal"] = blue
style.syntax["string"] = cyan
style.syntax["operator"] = fg
style.syntax["function"] = blue

style.guide = gray          -- indentguide